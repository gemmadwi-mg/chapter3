const { Client } = require('pg');

const client = new Client({
    user: 'postgres',
    host: 'localhost',
    password: 'gemmadwi',
    database: 'game',
    port: 5432,
});

function authenticate(userObj) {
    return ('gemma@mail.com' === userObj.email) && ('123456' === userObj.password);
}

function createDatabase(userObj, databaseName) {
    if (authenticate(userObj)) {
        const queryCreateDatabase = `CREATE DATABASE ${databaseName};`;
        client.connect();

        client.query(queryCreateDatabase, (err, res) => {
            if (err) {
                client.end();
                console.log(err);
            } else {
                client.end();
                console.log('Sukses membuat database');
            }
        });
    } else {
        console.log('Anda tidak memiliki hak!');
    }
}

function createTableUserGameBiodata(userObj) {
    if (authenticate(userObj)) {
        
        const queryCreateTable = `
        CREATE TABLE user_game_biodata (
            user_game_biodata_id SERIAL PRIMARY KEY,
            usia INT NOT NULL
        );
        `;
        
        client.connect();

        client.query(queryCreateTable, (err, res) => {
            if (err) {
                client.end();
                console.log(err);
            } else {
                client.end();
                console.log('Sukses membuat table user game biodata');
            }
        });

    } else {
        console.log('Anda tidak memiliki hak!');
    }
}

function createTableUserGameHistory(userObj) {
    if (authenticate(userObj)) {
        const queryCreateTable = `
        CREATE TABLE user_game_history (
            user_game_history_id SERIAL PRIMARY KEY,
            skor INT NOT NULL
        );
        `;
        client.connect();

        client.query(queryCreateTable, (err, res) => {
            if (err) {
                client.end();
                console.log(err);
            } else {
                client.end();
                console.log('Sukses membuat table user game history');
            }
        });

    } else {
        console.log('Anda tidak memiliki hak!');
    }
}

function createTableUserGame(userObj) {
    if (authenticate(userObj)) {
        const queryCreateTable = `
        CREATE TABLE user_game (
            user_game_id SERIAL PRIMARY KEY,
            user_game_biodata_id INT NOT NULL,
            user_game_history_id INT NOT NULL,
            username VARCHAR(50) UNIQUE NOT NULL,
            password TEXT NOT NULL,
            email VARCHAR(255) UNIQUE NOT NULL,
            CONSTRAINT fk_user_game_biodata
                FOREIGN KEY(user_game_biodata_id) 
	                REFERENCES user_game_biodata(user_game_biodata_id),
            CONSTRAINT fk_user_game_history
                FOREIGN KEY(user_game_history_id) 
	                REFERENCES user_game_history(user_game_history_id)
        );
        `;

        client.connect();

        client.query(queryCreateTable, (err, res) => {
            if (err) {
                client.end();
                console.log(err);
            } else {
                client.end();
                console.log('Sukses membuat table user game');
            }
        });

    } else {
        console.log('Anda tidak memiliki hak!');
    }
}

function insertDataUserGameBiodata(userObj) {
    if (authenticate(userObj)) {
        const queryInsertUserGameBiodata =
        `
        INSERT INTO user_game_biodata(usia) 
        VALUES(19);
        `;

        client.connect();

        client.query(queryInsertUserGameBiodata, (err, res) => {
            if (err) {
                client.end();
                console.log(err);
            } else {
                client.end();
                console.log('Sukses menambahkan data di table user_game_biodata');
            }
        });
    } else {
        console.log('Anda tidak memiliki hak!');
    }
}

function insertDataUserGameHistory(userObj) {
    if (authenticate(userObj)) {
        const queryInsertUserGameBiodata =
        `
        INSERT INTO user_game_history(skor) 
        VALUES(88);
        `;

        client.connect();

        client.query(queryInsertUserGameBiodata, (err, res) => {
            if (err) {
                client.end();
                console.log(err);
            } else {
                client.end();
                console.log('Sukses menambahkan data di table user_game_biodata');
            }
        });
    } else {
        console.log('Anda tidak memiliki hak!');
    }
}

function insertDataUserGame(userObj) {
    if (authenticate(userObj)) {
        const queryInsertUserGame =
        `
        INSERT INTO user_game(user_game_biodata_id,user_game_history_id,username, password, email) 
        VALUES(1, 1, 'gemplostorm', '123456', 'gemplo@mail.com');
        `;

        client.connect();

        client.query(queryInsertUserGame, (err, res) => {
            if (err) {
                client.end();
                console.log(err);
            } else {
                client.end();
                console.log('Sukses menambahkan data di table user_game');
            }
        });
    } else {
        console.log('Anda tidak memiliki hak!');
    }
}

function readDataUserGame(userObj) {
    if (authenticate(userObj)) {
        const queryCreateDatabase = `
        SELECT * FROM user_game 
        INNER JOIN user_game_biodata
        ON user_game.user_game_biodata_id = user_game_biodata.user_game_biodata_id
        INNER JOIN user_game_history
        ON user_game.user_game_history_id = user_game_history.user_game_history_id;
        `;

        client.connect();

        client.query(queryCreateDatabase, (err, res) => {
            if (err) {
                client.end();
                console.log(err);
            }
            client.end();
            console.log(res.rows);
        });
    } else {
        console.log('Anda tidak memiliki hak!');
    }
}

function updateDataUserGame(userObj) {
    if (authenticate(userObj)) {
        const queryCreateDatabase = `
        UPDATE user_game
        SET username = 'Hades'
        WHERE user_game_id = 1;`;

        client.connect();

        client.query(queryCreateDatabase, (err, res) => {
            if (err) {
                client.end();
                console.log(err);
            }
            client.end();
            console.log('Sukses update data table');
        });
    } else {
        console.log('Anda tidak memiliki hak!');
    }
}

function deleteDataUserGameBiodata(userObj) {
    if (authenticate(userObj)) {
        const queryCreateDatabase = `
        DELETE FROM user_game
        WHERE user_game_id = 1;
        `;
        client.connect();

        client.query(queryCreateDatabase, (err, res) => {
            if (err) {
                client.end();
                console.log(err);
            } else {
                client.end();
                console.log('Sukses hapus data');
            }


        });
    } else {
        console.log('Anda tidak memiliki hak!');
    }
}


const user = {
    email: 'gemma@mail.com',
    password: '123456'
}

// createDatabase(user, 'game');
// createTable(user);
// createTableUserGameBiodata(user);
// createTableUserGameHistory(user);
// createTableUserGame(user);
// insertDataUserGameBiodata(user);
// insertDataUserGameHistory(user);
// insertDataUserGame(user);
// readDataUserGame(user);
// updateDataUserGame(user);
// deleteDataUserGame(user);

/* 
Penjelasan
Langkah 1 :
Memanggil fungsi createDatabase dengan
memasukkan parameter userObj dan databaseName

Langkah 2 tambahkan di client key database value
nama dari database yang dibuat tadi

Membuat table

Langkah 3 : Memanggil fungsi createTableUserGameBiodata dengan 
parameter user untuk membuat table user_game_biodata

Langkah 4 : Memanggil fungsi createTableUserGameHistory dengan 
parameter user untuk membuat table user_game_history

Langkah 5 : Memanggil fungsi createTableUserGame dengan 
parameter user untuk membuat table user_game

Memasukkan Data

Langkah 6 : Memanggil fungsi insertDataTableUserGameBiodata dengan 
parameter user untuk memasukkan data ke table user_game_biodata

Langkah 7 : Memanggil fungsi insertDataTableUserGameHistory dengan 
parameter user untuk memasukkan data ke table user_game_history

Langkah 8 : Memanggil fungsi insertDataUserGame dengan 
parameter user untuk memasukkan data ke table user_game

Menampilkan Data 

Langkah 9 :  Memanggil fungsi readDataUserGame dengan 
parameter user untuk menampilkan data table user_game

Mengupdate Data 

Langkah 10 :  Memanggil fungsi updateDataUserGame dengan 
parameter user untuk mengupdate data table user_game

Menghapus Data 

Langkah 11 : Memanggil fungsi deleteDataUserGame dengan 
parameter user untuk menghapus data table user_game

*/



